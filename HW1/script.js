class Employee{
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }

    set name(newName) {
        this._name = newName
    }

    get age() {
        return this.age
    }

    set age(newAge) {
        this._age = newAge
    }

    get salary() {
        return this.salary
    }

    set salary(newSalary) {
        this._salary = newSalary
    }

}

class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age,salary);
        this._lang = lang
    }

    get experience() {
        return this.lang
    }

    set experience(newLang) {
        this.lang = newLang
    }

    get salary() {
        return this.salary
    }

    set salary(newSalary) {
        this.newSalary = newSalary * 3;
    }

}

const prog1 = new Programmer("Alex",25,1000,"JS")
const prog2 = new Programmer("Valera",25,2000,"JS")
const prog3 = new Programmer("Petro",25,1500,"Java")

console.log(prog1);
console.log(prog2);
console.log(prog3);