class findIP {

    constructor() {
        this.div = {
            container: document.createElement('div'),
            button: document.createElement('button')
        }
    }

    BASE_URL = 'https://api.ipify.org/?format=json'

    async getIP() {
        const { container } = this.div
        let IP = await fetch(this.BASE_URL)
            .then(res => res.json())
        let userInfo = await fetch(`http://ip-api.com/json/${IP.ip}?fields=continent,country,regionName,city,district`)
            .then(res => res.json())
        container.insertAdjacentHTML('afterend', `
            <p>${userInfo.continent}</p>
            <p>${userInfo.country}</p>
            <p>${userInfo.regionName}</p>
            <p>${userInfo.city}</p>
            <p>${userInfo.district}</p>`)
    }

    async render() {
        const { container, button } = this.div
        button.textContent = `Вычислить по IP`
        await button.addEventListener('click', () => this.getIP())

        container.append(button)
        document.body.append(container)
    }

}

let userInfo = new findIP()
userInfo.render()