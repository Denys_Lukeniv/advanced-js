const BASE_URL = 'https://ajax.test-danit.com/api/json';

class Card {
  updateInfo(userInfo) {
    this.id = userInfo.id;
    this.name = userInfo.name;
    this.username = userInfo.username;
    this.email = userInfo.email
  }

  addPosts(posts) {
    this.posts = posts;
  }

  deletePosts(posts) {
    this.posts = posts;
  }
}

start();

async function start() {
  let users = await getUsersWithPosts();
  await showInfoOnThePage(users);
  addSubscribeForDeleteButton();
}

async function getUsersWithPosts() {
  let data = [];

  let responseUsers = await fetch(`${BASE_URL}/users`);
  let users = await responseUsers.json();

  users.reverse().forEach(u => {
    let user = new Card();
    user.updateInfo(u);
    data.push(user);
  });

  let responsePosts = await fetch(`${BASE_URL}/posts`);
  let posts = await responsePosts.json();


  data.forEach(user => {
    let postByUser = posts
      .filter(p => p.userId == user.id)
      .map(p => {
        let post = {};
        post.title = p.title;
        post.body = p.body;
        post.id = p.id;
        return post;
      });

    user.addPosts(postByUser);
  });

  return data;
}



async function showInfoOnThePage(users) {
  users.forEach(user => {
    document.body.insertAdjacentHTML('afterbegin', `
      <div class="user container" id="user_${user.id}">
        <div class="user-info"> 
          <h4 class="user-info-text user-name">Name: ${user.name}</h2>
          <span class="user-info-text">@${user.username} </span>
          <span class="user-info-text">Email: ${user.email} </span>
            </div>
            ${user.posts.map(p => {
      return `
            <div class="user-posts" id ="${p.id}">
            <div class="post">
            <div class="post-head">
                <h6 class="post-title">${p.title}</h6>
                <button name="button" class="delete" data-user-id="${user.id}" data-id="${p.id}"> X </button>
            </div>
            <p class="post-body">${p.body}</p>
        </div>
            </div>
            `
    }).join('')}
        </div>
      `);
  });
}

function addSubscribeForDeleteButton() {
  let deleteButtons = document.body.querySelectorAll('.delete');
  deleteButtons.forEach(b => {
    b.addEventListener('click', (e) => {
      let target = e.target
      let post_id = target.getAttribute("data-id");
      let user_id = "user_" + target.getAttribute("data-user-id");
      let post = document.getElementById(post_id);
      let block_user = document.getElementById(user_id);

      fetch(`${BASE_URL}/posts/${post_id}`, {
        method: 'DELETE'
      }).then(response => {
        if (response.ok) {
          post.remove();

          if (block_user.children.length == 1) {
            block_user.remove();
          }
        }
      });
    });
  });
}