const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


let checkedList = books.map(({ author, name, price }) => {

    try {
        if (author && name && price) {
            return `
            <li> Author: ${author} </li>
            <li> Name: ${name} </li>
            <li class ="padding"> Price: ${price} </li> 
            `
        } else if (!author) {
            throw new Error('Книга не містить автора!')
        } else if (!name) {
            throw new Error('Книга не містить назви!')
        } else if (!price) {
            throw new Error('Книга не містить ціни!')
        }

    } catch (error) {
        console.error(`Error: ${error.message}`);
    }
})

function arrayToList(array, parrent = document.body){
    parrent.insertAdjacentHTML('beforeend', `<ul>${array.join(' ')}</ul>`)
}

arrayToList(checkedList,document.querySelector('#root'))