const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films'

fetch(filmsUrl)
    .then(res => res.json())

    .then(episodes => {
        const episodesContainer = document.createElement('ul')
        document.body.insertAdjacentElement('afterbegin', episodesContainer)
        episodes.forEach(episode => {
            const episodesList = document.createElement('li')
            const { episodeId, name, openingCrawl, characters } = episode;
            episodesList.insertAdjacentHTML('afterbegin', `
            <h1>Episode №${episodeId}: ${name} </h1>
            <p>${openingCrawl} </p>
            `);

            const request = characters.map(character => fetch(character));
            const episodeCharacters = document.createElement('ul')
            Promise.all(request)
                .then(res => Promise.all(res.map(r => r.json())))
                .then(characters => {
                    characters.forEach(character => {
                        const { name } = character;
                        episodeCharacters.insertAdjacentHTML('beforeend', `<li>${name}</li>`);
                    });
                });

                
            episodesList.append(episodeCharacters);
            episodesContainer.append(episodesList);

        });
    })


